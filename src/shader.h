#ifndef _Shader
#define _Shader
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "file.h"

GLuint
create_shader_program(const char *vert_path, const char *frag_path, const char *geom_path)
{
	GLuint program;
	GLuint vert_shader;
	GLuint frag_shader;
	GLuint geom_shader;
	char *vert_shader_source;
	char *frag_shader_source;
	char *geom_shader_source;
	int success;
	char info_log[512];

	read_file_into_buffer(&vert_shader_source, vert_path);

	printf("Creating Vertex Shader %s... ", vert_path);

	vert_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vert_shader, 1, (const char * const *)&vert_shader_source, NULL);
	glCompileShader(vert_shader);

	glGetShaderiv(vert_shader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
	glGetShaderInfoLog(vert_shader, 512, NULL, info_log);
	printf("[RENDERING] Error compiling vertex shader\n%s\n", info_log);
	return -1;
	}

	puts("Done.");

	read_file_into_buffer(&frag_shader_source, frag_path);

	printf("Creating Fragment Shader %s... ", frag_path);

	frag_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(frag_shader, 1, (const char * const *)&frag_shader_source, NULL);
	glCompileShader(frag_shader);

	glGetShaderiv(frag_shader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
	glGetShaderInfoLog(frag_shader, 512, NULL, info_log);
	printf("[RENDERING] Error compiling fragment shader\n%s\n", info_log);
	return -1;
	}

	puts("Done.");

	if (geom_path != NULL)
	{
	read_file_into_buffer(&geom_shader_source, geom_path);

	printf("Creating Geometry Shader %s... ", geom_path);

	geom_shader = glCreateShader(GL_GEOMETRY_SHADER);
	glShaderSource(geom_shader, 1, (const char * const *)&geom_shader_source, NULL);
	glCompileShader(geom_shader);

	glGetShaderiv(geom_shader, GL_COMPILE_STATUS, &success);

	if (!success)
	{
	    glGetShaderInfoLog(geom_shader, 512, NULL, info_log);
	    printf("[RENDERING] Error compiling geometry shader\n%s\n", info_log);
	    return -1;
	}

	puts("Done.");
	}

	printf("Creating Shader Program... ");

	program = glCreateProgram();

	puts("Done.");

	printf("Linking Shader Program %d... ", program);

	glAttachShader(program, vert_shader);
	glAttachShader(program, frag_shader);
	if (geom_path != NULL)
	glAttachShader(program, geom_shader);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);

	if (!success)
	{
	glGetProgramInfoLog(program, 512, NULL, info_log);
	printf("[RENDERING] Error linking shader\n%s\n", info_log);
	return -1;
	}

	puts("Done.");

	return program;
}
#endif
