#include <stdio.h>
#include <stdlib.h>

int read_file_into_buffer(char **buffer, const char *path)
{
	int fsize;
	char *str;
	FILE *file;

	printf("Reading %s into buffer... \n", path);

	file = fopen(path, "rb");
	fseek(file, 0, SEEK_END);
	fsize = ftell(file);
	fseek(file, 0, SEEK_SET);

	str = (char*)malloc(fsize + 1);
	fread(str, 1, fsize, file);
	fclose(file);
	str[fsize] = 0;

	*buffer = str;

	puts("Done.");
	return 0;
}
