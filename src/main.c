#include <assert.h>
#include "ash.h"

/* GLOBALS ================================================================== */

#define PI 3.1415926
#define SCR_WIDTH 384
#define SCR_HEIGHT 216
#define SCR_SCALE 5
#define CLEAR_COLOUR 0.1f, 0.1f, 0.1f, 1.0f

GLFWwindow *window;
GLuint text_shader,	ui_shader,	sprite_shader;
GLuint text_vao,	ui_vao,		sprite_vao;
GLuint text_vbo,	ui_vbo,		sprite_vbo;
GLuint 			ui_ebo,		sprite_ebo;
FT_Library ft;
struct font fonts[32];
int font_count = 0;
mat4x4 model_normal = {
	{ 1, 0, 0, 0 },
	{ 0, 1, 0, 0 },
	{ 0, 0, 1, 0 },
	{ 0, 0, 0, 1 },
};
struct sprite sprites[64] = {0};
unsigned sprite_count = 0;

void (*scene)();

void error_callback(int error, const char* description)
{
	fprintf(stderr, "Error: %s\n", description);
}

void create_window(const char *name)
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);

	glfwSetErrorCallback(error_callback);

	window = glfwCreateWindow(SCR_WIDTH * SCR_SCALE, SCR_HEIGHT * SCR_SCALE, name, NULL, NULL);

	assert(window);

	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetKeyCallback(window, key_callback);

	assert(gladLoadGLLoader((GLADloadproc)glfwGetProcAddress));
}

void create_text_shader()
{
	int i = 0;
	mat4x4 projection;

	text_shader = create_shader_program("src/glyph.vs", "src/glyph.fs", NULL);
	glUseProgram(text_shader);
	mat4x4_ortho(projection, 0, SCR_WIDTH * SCR_SCALE, 0, SCR_HEIGHT * SCR_SCALE, -1.0, 1.0);
	setUniformMatrix4fv(&text_shader, "projection", &projection[0][0]);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	if (FT_Init_FreeType(&ft)) {
		printf("failed to init freetype\n");
		exit(-1);
	}

	for (i = 0; i < font_count; ++i) {
		GLubyte c = 0;
		int g = 0;
		FT_Face face;

		if (FT_New_Face(ft, fonts[i].path, 0, &face)) {
			printf("failed to load %s\n", fonts[i].path);
			exit(-1);
		}

		FT_Set_Pixel_Sizes(face, 0, fonts[i].size);

		for (c = 0; c < 128; ++c) {
			GLuint texture;

			if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
				printf("Failed to load glyph '%c'\n", c);
				continue;
			}

			glGenTextures(1, &texture);
			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D,
				     0,
				     GL_RED,
				     face->glyph->bitmap.width,
				     face->glyph->bitmap.rows,
				     0,
				     GL_RED,
				     GL_UNSIGNED_BYTE,
				     face->glyph->bitmap.buffer);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

			fonts[i].glyphs[g].texture_id = texture;
			fonts[i].glyphs[g].width = face->glyph->bitmap.width;
			fonts[i].glyphs[g].height = face->glyph->bitmap.rows;
			fonts[i].glyphs[g].bearing_left = face->glyph->bitmap_left;
			fonts[i].glyphs[g].bearing_top = face->glyph->bitmap_top;
			fonts[i].glyphs[g].advance = face->glyph->advance.x;

			++g;
		}
		glBindTexture(GL_TEXTURE_2D, 0);
		FT_Done_Face(face);
	}

	FT_Done_FreeType(ft);

	glGenVertexArrays(1, &text_vao);
	glGenBuffers(1, &text_vbo);
	glBindVertexArray(text_vao);
	glBindBuffer(GL_ARRAY_BUFFER, text_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), NULL);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void create_ui_shader()
{
	mat4x4 projection;
	mat4x4 model;
	GLfloat vertices[] = {
		1.0f,  1.0f, 0.0f,
		1.0f, -0.0f, 0.0f,
		-0.0f, -0.0f, 0.0f,
		-0.0f,  1.0f, 0.0f,
	};
	GLint indices[] = {
		0, 1, 3,
		1, 2, 3
	};
	ui_shader = create_shader_program("src/ui.vs", "src/ui.fs", NULL);
	mat4x4_add(projection, projection, model_normal);
	mat4x4_add(model, model, model_normal);

	glGenVertexArrays(1, &ui_vao);
	glGenBuffers(1, &ui_vbo);
	glGenBuffers(1, &ui_ebo);

	glBindVertexArray(ui_vao);
	glBindBuffer(GL_ARRAY_BUFFER, ui_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ui_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	mat4x4_ortho(projection, 0, SCR_WIDTH, 0, SCR_HEIGHT, -1.0f, 1.0f);

	glUseProgram(ui_shader);

	setUniformMatrix4fv(&ui_shader, "projection", &projection[0][0]);
	setUniformMatrix4fv(&ui_shader, "model", &model[0][0]);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void create_sprite_shader()
{
	mat4x4 projection;
	GLfloat vertices[] = {
		/* vertices		colours				tex coords */
		0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 1.0f,
		0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	1.0f, 0.0f,
		-0.5f, -0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 0.0f,
		-0.5f,  0.5f, 0.0f,	1.0f, 1.0f, 1.0f, 1.0f, 	0.0f, 1.0f
	};
	GLuint indices[] = {
		0, 1, 3,
		1, 2, 3
	};

	glGenVertexArrays(1, &sprite_vao);
	glGenBuffers(1, &sprite_vbo);
	glGenBuffers(1, &sprite_ebo);

	glBindVertexArray(sprite_vao);

	glBindBuffer(GL_ARRAY_BUFFER, sprite_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_ebo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	sprite_shader = create_shader_program("src/sprite.vs", "src/sprite.fs", NULL);

	glUseProgram(sprite_shader);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), NULL);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(3 * sizeof(GLfloat)));
	glEnableVertexAttribArray(1);

	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 9 * sizeof(GLfloat), (void*)(7 * sizeof(GLfloat)));
	glEnableVertexAttribArray(2);

	mat4x4_ortho(projection, 0, SCR_WIDTH, 0, SCR_HEIGHT, -1.0f, 1.0f);
	setUniformMatrix4fv(&sprite_shader, "projection", &projection[0][0]);
}

GLfloat render_text(const char *text, struct font *font, GLfloat x, GLfloat y)
{
	int i;
	int len = strlen(text);

	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(text_shader);
	setUniform4f(&text_shader, "text_colour", 1, 1, 1, 1);
	glActiveTexture(GL_TEXTURE0);
	glBindVertexArray(text_vao);

	for (i = 0; i < len; ++i) {
		struct glyph g = font->glyphs[(int)text[i]];

		float xpos = x + g.bearing_left;
		float ypos = y - (g.height - g.bearing_top);

		float w = g.width;
		float h = g.height;

		float vertices[6][4] = {
			{ 0, 0, 0, 0 },
			{ 0, 0, 0, 1 },
			{ 0, 0, 1, 1 },
			{ 0, 0, 0, 0 },
			{ 0, 0, 1, 1 },
			{ 0, 0, 1, 0 },
		};

		vertices[0][0] = xpos;
		vertices[0][1] = ypos + h;

		vertices[1][0] = xpos;
		vertices[1][1] = ypos;

		vertices[2][0] = xpos + w;
		vertices[2][1] = ypos;

		vertices[3][0] = xpos;
		vertices[3][1] = ypos + h;

		vertices[4][0] = xpos + w;
		vertices[4][1] = ypos;

		vertices[5][0] = xpos + w;
		vertices[5][1] = ypos + h;

		glBindTexture(GL_TEXTURE_2D, g.texture_id);
		glBindBuffer(GL_ARRAY_BUFFER, text_vbo);
		glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof vertices, vertices);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDrawArrays(GL_TRIANGLES, 0, 6);
		x += (g.advance >> 6);
	}

	glBindVertexArray(0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	return x;
}

void render_text_wrapped(const char *text, struct font *font, GLfloat x, GLfloat y, GLfloat width, GLfloat height)
{
	char str[4096];
	char delimiter[2] = " ";
	char *token;
	GLfloat cursor_x = x;
	GLfloat cursor_y = y;

	strcpy(str, text);

	token = strtok(str, delimiter);

	while (NULL != token) {
		int len = strlen(token);

		if (cursor_x + (font->size / 2) * len > x + width) {
			cursor_x = x;
			cursor_y -= font->size * font->line_height;
			continue;
		}

		if (cursor_y < y - height) {
			break;
		}

		cursor_x = render_text(" ", font, cursor_x, cursor_y);
		cursor_x = render_text(token, font, cursor_x, cursor_y);
		token = strtok(NULL, delimiter);
	}
}

void render_quad(struct colour *colour, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height)
{
	mat4x4 model;
	mat4x4_add(model, model, model_normal);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(ui_shader);
	glBindVertexArray(ui_vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ui_ebo);

	mat4x4_translate(model, x, y, z);
	mat4x4_scale_aniso(model, model, width, height, 1);

	setUniformMatrix4fv(&ui_shader, "model", &model[0][0]);
	setUniform4f(&ui_shader, "colour", colour->r, colour->g, colour->b, colour->a);

	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	glDisable(GL_BLEND);
}

void renderer_init()
{
	stbi_set_flip_vertically_on_load(1);
	create_window("ASH TEST");
	glClearColor(CLEAR_COLOUR);
	create_text_shader();
	create_ui_shader();
	create_sprite_shader();
}

void font_add(const char *path, int size, float line_height)
{
	fonts[font_count].path = path;
	fonts[font_count].size = size;
	fonts[font_count].line_height = line_height;
	font_count++;
}

unsigned sprite_add(const char *path)
{
	int w, h, nc;
	int id = sprite_count;
	unsigned char *data = stbi_load(path, &w, &h, &nc, 0);

	if (!data) {
		printf("failed to load texture %s\n", path);
		exit(-1);
	}

	glUseProgram(sprite_shader);
	glBindVertexArray(sprite_vao);
	glBindBuffer(GL_ARRAY_BUFFER, sprite_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, sprite_vao);

	glGenTextures(1, &sprites[id].texture_id);
	glBindTexture(GL_TEXTURE_2D, sprites[id].texture_id);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(data);

	sprites[id].width = w;
	sprites[id].height = h;

	sprite_count++;

	return id;
}

void render_sprite(struct sprite *sprite, GLfloat x, GLfloat y, GLfloat z, GLfloat rot)
{
	mat4x4 transform;

	mat4x4_identity(transform);
	mat4x4_translate(transform, x, y, z);
	mat4x4_rotate(transform, transform, 0, 0, 1.0f, rot);
	mat4x4_scale_aniso(transform, transform, sprite->height, sprite->width, 1.0f);
	setUniformMatrix4fv(&sprite_shader, "transform", &transform[0][0]);

	glUseProgram(sprite_shader);
	glBindVertexArray(sprite_vao);

	glBindTexture(GL_TEXTURE_2D, sprite->texture_id);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

/* client code below */

void framebuffer_size_callback(GLFWwindow *window, int width, int height)
{
	glViewport(0, 0, width, height);
}

void scene_a()
{
	struct colour red = { 1.0f, 0, 0, 1.0f };
	render_sprite(&sprites[0], 20, 20, 0, 0);
	render_text("What do you think?", &fonts[0], (SCR_WIDTH * SCR_SCALE)/2, 50);
	render_text_wrapped("Yeah, this is pretty great.", &fonts[1], 50, 200, 100, 200);
	render_quad(&red, 100, 100, 0, 50, 50);
}

void scene_b()
{
	render_text("Scene B, baby!", &fonts[0], 200, 200);
}

void process_input(GLFWwindow *window)
{
	if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_ESCAPE))
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_S && action == GLFW_PRESS)
		scene = scene == scene_a ? scene_b : scene_a;
}

int main(void)
{
	font_add("assets/fonts/Roboto.ttf", 40, 1.2f);
	font_add("assets/fonts/Roboto.ttf", 16, 1.2f);
	renderer_init();
	sprite_add("assets/player.png");
	scene = scene_a;

	while (!glfwWindowShouldClose(window)) {
		process_input(window);
		glClear(GL_COLOR_BUFFER_BIT);

		/* start render */
		scene();
		/* end render */

		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwTerminate();
	return 0;
}
