#version 460 core
out vec4 fragment; 

uniform vec4 colour;

void main()
{
    fragment = colour;
}
