#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

void
setUniform1i(const GLuint *program, const char *name, const GLint value)
{
    GLuint loc = glGetUniformLocation(*program, name);
    glUniform1i(loc, value);
}

void
setUniform3f(
    const GLuint *program,
    const char *name,
    const float a,
    const float b,
    const float c)
{
    GLuint loc = glGetUniformLocation(*program, name);
    glUniform3f(loc, a, b, c);
}

void
setUniform4f(
    const GLuint *program,
    const char *name,
    const float a,
    const float b,
    const float c,
    const float d)
{
    GLuint loc = glGetUniformLocation(*program, name);
    glUniform4f(loc, a, b, c, d);
}

void
setUniform4fv(const GLuint *program, const char *name, const GLfloat *value)
{
	GLuint loc = glGetUniformLocation(*program, name);
	glUniform4fv(loc, 1, value);
}

void
setUniformMatrix4fv(const GLuint *program, const char *name,
		    const GLfloat *value)
{
    GLuint loc = glGetUniformLocation(*program, name);
    glUniformMatrix4fv(loc, 1, GL_FALSE, value);
}
