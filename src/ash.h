#ifndef ash_h_INCLUDED
#define ash_h_INCLUDED
#define GLFW_DLL
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <ft2build.h>
#include FT_FREETYPE_H

#include "math_linear.h"
#include "shader.h"
#include "gl_helpers.h"
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

struct glyph {
	GLuint texture_id;
	GLint width;
	GLint height;
	GLint bearing_left;
	GLint bearing_top;
	GLint advance;
};

struct font {
	const char *path;
	int size;
	float line_height;
	struct glyph glyphs[128];
};

struct sprite {
	GLuint texture_id;
	GLfloat width;
	GLfloat height;
};

struct colour {
	float r;
	float g;
	float b;
	float a;
};

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void key_callback(GLFWwindow *window, int key, int scancode, int action, int mods);
void render_sprite(struct sprite *sprite, GLfloat x, GLfloat y, GLfloat z, GLfloat rot);
GLfloat render_text(const char *text, struct font *font, GLfloat x, GLfloat y);
void render_text_wrapped(const char *text, struct font *font, GLfloat x, GLfloat y, GLfloat width, GLfloat height);
void render_quad(struct colour *colour, GLfloat x, GLfloat y, GLfloat z, GLfloat width, GLfloat height);

#endif

